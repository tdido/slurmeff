import datetime
import json

import sqlite3

import numpy as np
import pandas as pd
import plotly.express as px
from dash import Dash, Input, Output, callback, dcc, html
import plotly.graph_objs as go

app = Dash(__name__)

def do_init():
    cnx = sqlite3.connect('data/slurmeff.db')
    cur = cnx.cursor()

    cur.execute("SELECT min(End),  max(end) FROM jobs")
    rows = cur.fetchone()

    min_date = rows[0]
    max_date = rows[1]

    cur.execute("SELECT DISTINCT(User) FROM jobs")
    rows = cur.fetchall()
    users = [row[0] for row in rows]

    cnx.commit()
    cnx.close()

    return html.Div(
        style={"font-family": "sans-serif"},

        children = [

        # first row
        html.Div(children=[
            html.H1(children="Slurm usage efficiency", style={"textAlign": "center"}),

        ]),

        # second row
        html.Div(children=[
            html.Label("User:", style={"font-weight":"bold"}),
            dcc.Dropdown(users, id="dd-username", placeholder="Select a user..."),
        ]),

        html.Div(children=[
            html.Label("Date range:", style={"font-weight":"bold"}),
            html.Br(),
            dcc.DatePickerRange(
                id="dp-dates",
                min_date_allowed=min_date,
                max_date_allowed=max_date,
                initial_visible_month=max_date,
                start_date=datetime.date.today()-datetime.timedelta(days=7),
                end_date=max_date,
                month_format="DD/MM/YYYY",
                display_format="DD/MM/YYYY",
            ),
        ]),

        html.Div(children=[
            html.Label("Job name:", style={"font-weight":"bold"}),
            html.Br(),
            dcc.Input(id="txt-jobname", placeholder="Enter part of the job name...", type="text", debounce=1),
        ], className='row'),

        html.Div(children=[
            html.Label("Resource types:", style={"font-weight":"bold"}),
            dcc.Checklist(
                options={
                    'MemEff': 'Mem',
                    'CPUEff': 'CPU',
                    'TimeEff': 'Time'
                },
                value=['MemEff','CPUEff','TimeEff'],
                inline=True,
                id="cb-fields"
            ),
        ]),

        html.Div(children=[
            html.Br(),
            dcc.Checklist(["Show separate graphs"],["Show separate graphs"], id="cb-separate"),
        ]),

        html.Div(children=[
            html.H2(children="message", style={"textAlign": "center"}, id="message"),
            html.H3(children="memeff", style={"textAlign": "center"}, id="memeff"),
            html.H3(children="cpueff", style={"textAlign": "center"}, id="cpueff"),
            html.H3(children="timeeff", style={"textAlign": "center"}, id="timeeff"),
        ]),

        html.Div(children=[
            dcc.Graph(id="graph-content"),
        ]),

    ])

def emptyplot():
    fig = go.Figure()
    fig.update_layout(
        height=500,
        xaxis =  { "visible": False },
        yaxis = { "visible": False },
        plot_bgcolor = "white",
        annotations = [
            {
                "text": "",
                "showarrow": False,
                "font": {
                    "size": 28
                }
            }
        ]
    )

    return fig


app.layout = do_init

@callback(
    Output("graph-content", "figure"),
    Output("message", "children"),
    Output("memeff", "children"),
    Output("cpueff", "children"),
    Output("timeeff", "children"),
    Input("dd-username", "value"),
    Input("dp-dates", "start_date"),
    Input("dp-dates", "end_date"),
    Input("txt-jobname", "value"),
    Input("cb-separate", "value"),
    Input("cb-fields", "value"),
)
def update_graph(username, start_date, end_date, jobname_filter, separate, fields):
    message=None
    memeff=None
    cpueff=None
    timeeff=None
    start_date = datetime.datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.datetime.strptime(end_date, '%Y-%m-%dT%H:%M:%S')

    cnx = sqlite3.connect('data/slurmeff.db')
    if jobname_filter:
        df = pd.read_sql_query("SELECT * FROM jobs WHERE JobName LIKE ? AND User = ? AND End BETWEEN ? AND ?", cnx, params=[f"%{jobname_filter}%",username, start_date, end_date])
    else:
        df = pd.read_sql_query("SELECT * FROM jobs WHERE User = ? AND End BETWEEN ? AND ?", cnx, params=[username, start_date, end_date])
    cnx.commit()
    cnx.close()

    df["MaxRSS"] = df["MaxRSS"].apply(lambda x: x[:-1] if x != "NaN" else x).astype(float)
    df["ReqMem"] = df["ReqMem"].str[:-1].astype(int)
    df["Submit"] = pd.to_datetime(df["Submit"], format="%Y-%m-%dT%H:%M:%S")
    df["End"] = pd.to_datetime(df["End"], format="%Y-%m-%dT%H:%M:%S")

    df["MemEff"] = df["MemEff"].astype(float)
    df["CPUEff"] = df["CPUEff"].astype(float)
    df["TimeEff"] = df["TimeEff"].astype(float)

    df["NameID"] = df["JobID"].astype(str) + "." + df["JobName"]

    mem_mean = df.MemEff.mean()
    cpu_mean = df.CPUEff.mean()
    time_mean = df.TimeEff.mean()

    target_eff = 80

    memeff = html.Span(f"Mean memory efficiency: {mem_mean:.0f}%", style={"color":"red"} if mem_mean < target_eff else None)
    cpueff = html.Span(f"Mean CPU efficiency: {cpu_mean:.0f}%", style={"color":"red"} if cpu_mean < target_eff else None)
    timeeff = html.Span(f"Mean time efficiency: {time_mean:.0f}%", style={"color":"red"} if time_mean < target_eff else None)

    if not username:
        return emptyplot(), "Please select a user to display data", None, None, None
    else:
        row_limit = 100
        message = ""

        if len(df) == 0:
            return emptyplot(), f"No data for {username} in the selected date range", None, None, None

        if len(df) > row_limit:
            nrows = len(df)
            df = df.head(row_limit)
            message = f"Showing first {row_limit} jobs (out of {nrows})"
        else:
            message = f"Showing all jobs for selected period"

        df = pd.melt(
            df,
            id_vars=[
                "NameID",
                "JobID",
                "JobName",
                "User",
                "Submit",
                "End",
                "ReqMem",
                "MaxRSS",
            ],
            value_vars=fields,
            var_name="efftype",
            value_name="effval",
        )

        facet = "efftype" if separate else None

        mult = 15 if separate else 5

        mode = "relative" if separate else "group"

        fig = px.bar(
            df,
            y="NameID",
            x="effval",
            color="efftype",
            barmode=mode,
            facet_row=facet,
            orientation="h",
            height=600 + mult * len(df),
            hover_name="NameID",
            hover_data=["MaxRSS", "ReqMem"],
        )

        fig.update_xaxes(matches=None)
        fig.for_each_xaxis(lambda xaxis: xaxis.update(showticklabels=True, tickangle=-45, range=[0,100]))
        fig.for_each_annotation(lambda a: a.update(textangle=0, text=a.text.split("=")[-1].rstrip("Eff")))

        return fig, message, memeff, cpueff, timeeff

if __name__ == "__main__":
    app.run_server(debug=True)
