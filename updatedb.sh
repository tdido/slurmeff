#!/bin/bash

HOURS=$1
DBFILE=$2

display_help() {
  echo "This script requires two parameters: HOURS and DBFILE"
  echo -e "\nUsage:\n$0 HOURS DBFILE\n"
  echo "HOURS: (integer) the script will report from this many hours ago until the current time."
  echo "DBFILE: (string) the path to the SQLite database file."
  echo -e "\nExample:\n$0 10 myDbFile.db"
  exit 1
}

if [[ ( $1 == "--help") ||  $1 == "-h" ]]
then
  display_help
  exit 0
fi

if [ -z "$HOURS" ] || [ -z "$DBFILE" ]; then
  echo "Usage: $0 HOURS DBFILE"
  exit 1
fi

sqlite3 $DBFILE "CREATE TABLE IF NOT EXISTS jobs ( JobID INTEGER PRIMARY KEY, JobName TEXT, State TEXT, User TEXT, Submit TEXT, End TEXT, Timelimit TEXT, Elapsed TEXT, TimeEff REAL, CPUEff REAL, ReqMem TEXT, MaxRSS TEXT, MemEff REAL);"
sqlite3 $DBFILE "CREATE INDEX IF NOT EXISTS idx_user ON jobs (User)"
sqlite3 $DBFILE "CREATE INDEX IF NOT EXISTS idx_submit ON jobs (Submit)"
sqlite3 $DBFILE "CREATE INDEX IF NOT EXISTS idx_end ON jobs (End)"
sqlite3 $DBFILE "DELETE FROM jobs WHERE DATE(End) < DATE('now','-1 year');"
sqlite3 $DBFILE ".mode csv"
reportseff \
    --since "h=$HOURS" \
    --format='JobID,JobName,State,User,Submit,End,TimeLimit,Elapsed,TimeEff,CPUEff,ReqMem,MaxRSS,MemEff' \
    -s COMPLETED \
    --extra-args='--units M' \
    -p | sed 's/---/NaN/g' | sed 's/||/|NaN|/g' | sqlite3 $DBFILE ".import --skip 1 '|cat -' jobs"
